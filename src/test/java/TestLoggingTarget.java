import io.openraven.eventlogging.LogEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestLoggingTarget {
    private static final Logger LOG = LoggerFactory.getLogger(EventLoggingTest.class);

    @LogEvent(message = "targetMethod")
    public String targetMethod(String s, int i){
        LOG.info("In targetMethod({}, {})", s, i);
        String ret = s + ":" + i;
        LOG.info("targetMethod({}, {}) returning {}", s, i, ret);
        return ret;
    }
}
