import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventLoggingTest {
    private static final Logger LOG = LoggerFactory.getLogger(EventLoggingTest.class);

    @Test
    public void TestLogging(){
        LOG.info("In 'TestLogging() - calling targetMethod(\"foo\", 1)");
        TestLoggingTarget target = new TestLoggingTarget();
        String str = target.targetMethod("foo", 1);

        LOG.info("In 'TestLogging() - targetMethodCall returned \"{}\"", str);

        Assertions.assertEquals("foo:1", str);

    }
}
