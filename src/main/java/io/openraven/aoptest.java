package io.openraven;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class aoptest {

    private static final Logger LOG = LoggerFactory.getLogger(aoptest.class);

    @Before("execution(* *(..))")
    public void aopTestHook() {
        LOG.info("BEHOLD the power of AOP !!!");
    }
}
