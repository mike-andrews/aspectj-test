package io.openraven;

import io.openraven.eventlogging.EventLogger;
import io.openraven.eventlogging.LogEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
    private static final Logger LOG = LoggerFactory.getLogger(App.class);
    
    public static void main( String[] args )
    {
        LOG.info("Starting in main(...)");
        foo();

    }

    @LogEvent(message = "foo")
    public static void foo() {
        LOG.info("in foo()...");
    }
}
