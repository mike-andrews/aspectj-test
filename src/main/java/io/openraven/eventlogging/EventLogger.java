package io.openraven.eventlogging;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

@Aspect
public class EventLogger {

    private static final Logger LOG = LoggerFactory.getLogger(EventLogger.class);

    @AfterReturning("execution(* *(..)) && @annotation(io.openraven.eventlogging.LogEvent)")
    public void logAfterAnnotatedMethodReturns(JoinPoint joinPoint) throws IllegalAccessException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        LogEvent annotations = method.getAnnotation(LogEvent.class);

        LOG.info("*** Advice called with message '{}'", annotations.message());
    }


}
